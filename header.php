<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">-->

    <title>Perio Gabinet</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!--    fonts-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
</head>

<body>

<?php

$class = "";

if(($_SERVER['REQUEST_URI']) == '/fur-arbeitgeber.php' || ($_SERVER['REQUEST_URI']) == '/contact.php'){
    $class = "footer-fixed-black";
    $socials = "d-none";
    $color = "color-black";
}else{
    $class = "";
}
?>


<div class="navbar__wrapper">
    <div class="container">
        <nav class="navbar navbar-expand-lg d-flex align-items-center menu">
            <a class="navbar-brand" href="/"><img src="img/logo-claim.svg" alt="Perio Gabinet"></a>

            <div class="nav__text">
                <p>Od 27 lat w wyjątkowy sposób dbamy<br>
                    o zdrowie i uśmiechy naszych pacjentów.</p>
            </div>
        </nav>
    </div>
</div>